from flask import Flask, Response, render_template, request, jsonify
import time, json, logging

app = Flask(__name__, template_folder="static/templates")


@app.route("/", methods=["GET"])
def home():
    user = dict()
    user["name"] = "User"
    return render_template("main.html", user=user)


@app.route("/result", methods=["POST"])
def get_result():
    body = request.get_json()
    time.sleep(2)
    message = body.get("message")
    if message is not None:
        return jsonify({"reply": message})
    else:
        return jsonify({"reply": "Expecing message key in body"})


if __name__ == "__main__":
    app.run("127.0.0.1", 8080)
